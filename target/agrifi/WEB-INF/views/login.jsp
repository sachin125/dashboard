<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<html xmlns:th="http://www.thymeleaf.org" xmlns:tiles="http://www.thymeleaf.org">
<title>AGRIFI</title>
	<head>
			<link rel="stylesheet" 	href="resources/css/style.css">
		<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
		<link href='http://fonts.googleapis.com/css?family=Oswald' rel='stylesheet' type='text/css' />
		<script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.min.js"></script>
		<link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css">
		<link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/font-awesome/4.2.0/css/font-awesome.min.css">
		<script src="http://ajax.aspnetcdn.com/ajax/jquery.validate/1.14.0/jquery.validate.js"></script>
		<link rel="shortcut icon" type="image/png" href="resources/images/favicon.ico"/>
		<script src="resources/js/sweetalert-dev.js"></script>
     <link rel="stylesheet" href="resources/css/sweetalert.css">
     <script src="resources/js/g-analytics.js"></script>
	</head>
	<style>
	body {
	height: 100%;
	background-color: hsla(200,40%,30%,.4);
	background-image:
		url(''),
		url('');
	background-position:
		0 20%,
		0 95%,
		0 0,
		0 100%,
		0 0;
	background-size:
		2500px,
		800px,
		500px 200px,
		1000px,
		400px 260px;
	}

	.purple-label {
		color: #9b3700;
		font-weight: bold;
		font-size: 22px;
		margin: 10px;
		text-align: center;
	}
	@keyframes para {
		100% {
			background-position:
				-5000px 20%,
				-800px 95%,
				500px 0,
				1000px 100%,
				400px 0;
			}
		}
	.login-box{
	   border: 1px solid #3e3d3d80;
		padding: 10px;
		margin: 5% 10px 10px;
		border-radius: 5px;
		background-color: rgba(255, 255, 255, 0.91);
		box-shadow: 2px 2px 5px 2px #41383880;
	}
	.input-lg{
		border-radius:2px;
	}
	.navbar-default {
	    background-color: rgba(149, 164, 88, 0.95);
		border-color: #95A458;
		height: 50px;
		color: #FFF;
	}
	.login-header{
		margin: 0 auto;
		text-align: center;
		font-size: 16px;
		padding: 10px;
		margin: 20px 0px 20px 0px;
		border: 1px solid #ddd;
		color: #fff;
		font-weight: bold;
		background-color: #95A458;
	}

	#saveUserCompany label{
		font-size: 10px;
		color: #C01414;
		font-weight: normal;
		margin-bottom: -10px;
		margin-left: 5px;
		margin-top: 5px;
	}

	.register-link{
		cursor:pointer;
		margin-left:10px;
	}
	.loading-pulse {
		background-color: rgba(0, 0, 0, 0.53);
	    z-index: 100;
	    position: absolute;
	    width: 100%;
	    height: 100%;
	    top: 0px;
	}
	.loading-text {
		color: #FFF;
		font-size: 14px;
		text-align: center;
		top: 50%;
		position: absolute;
		left: 50%;
		font-weight: bold;
	}
	.has-feedback .form-control-feedback{
		top:0px !important;
	}
	.glyphicon-ok::before{
	    color: #2A8421;
	}
	.glyphicon-remove::before {
    	color: #A11F1F;
	}
	.error-msg{
	    border: 1px solid #C96F5C;
	    padding: 10px;
	    background-color: rgba(210, 24, 24, 0.6);
	    margin: 10px;
	    border-radius: 8px;
	    text-align: center;
	    color: #fff;
	    font-weight: bold;
	    box-shadow: 2px 2px 2px rgba(203, 39, 39, 0.45);
	}
	.close-btn{
		border: 2px solid rgb(204, 204, 204);
		padding: 3px 7px;
	}
	.modal-footer{
		padding: 15px;
		text-align: right;
		border-top: 0px solid #E5E5E5;
	}
	.modal-header{
		min-height: 16.43px;
		padding: 15px;
		border-bottom: 0px solid #E5E5E5;
	}

.btn-primary {
    color: #FFF;
    background-color: #95A458;
    border-color: #95A458;
}
.btn-default {
    color: #FFF;
	background-color: #ca4800;
	border-color: #ca4800;
	width: 150px;
	border-radius: 2px;
	font-weight: bold;
	height: 40px;
}
.btn-default:hover {
    color: #FFF;
	background-color: #ca4800;
	border-color: #ca4800;
	width: 150px;
	border-radius: 2px;
	font-weight: bold;
	height: 40px;
}
.account-havetxt {
	font-size: 14px;
	padding: 10px;
	text-align: center;
}

video {
  position: absolute;
  top: 50%;
  left: 50%;
  min-height: 100%;
  min-width: 100%;
  z-index: -1;
  transform: translateY(-50%) translateX(-50%);
}

.button--loading::after {
         content: "";
         position: absolute;
         width: 16px;
         height: 16px;
         top: 0;
         left: 0;
         right: 0;
         bottom: 0;
         margin: auto;
         border: 16px solid #f3f3f3;
           border-radius: 50%;
           border-top: 16px solid #3498db;
           width: 75px;
           height: 75px;
           -webkit-animation: spin 2s linear infinite; /* Safari */
           animation: spin 2s linear infinite;
       }
       @keyframes button-loading-spinner {
         from {
           transform: rotate(0turn);
         }

         to {
           transform: rotate(1turn);
         }
       }

        .button--loading .button {
                              visibility: hidden;
                              opacity: 0;
                              background-color:#FF0000;
                            }

                            @keyframes spin {
                              0% { transform: rotate(0deg); }
                              100% { transform: rotate(360deg); }
                            }

	</style>
	<script type="text/javascript">
    </script>
  <body>
	<!-- <nav class="navbar navbar-default">
		<div style="text-align: center;font-size: 26px;font-weight: bold;">PAD</div>
	</nav> -->

	<video autoplay muted loop id="myVideo">
      <source src="http://www.zedeyelabs.com/wp-content/uploads/2017/11/business-meeting-room.mp4" type="video/mp4">
    </video>

  <div class="container-fluid">

  	<div class="col-md-4"></div>
	  	<div class="col-md-4 login-box">
        <div style="margin: 10px;text-align: center;">
            <div class="purple-label">AGRIFI DASHBOARD</div>
        </div>
        <div class="container-fluid">
	  	<div tiles:fragment="content" class="login-box1">
        <form method="post" th:action="@{/loginJSON}" name="f">

                 <b>${msg}</b>
         <fieldset>
             <!-- <div class="error-msg">Invalid username or password.</div> -->
             <input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}"/>
             <div class="margin-top-10  form-group" style="margin-top: 15px">
                <input class="form-control input-lg" type="text" id="username" name="username" placeholder="Username" />
             </div>
                 <div class="margin-top-10 form-group">
                <input class="form-control input-lg" type="password" id="password" name="password" placeholder="Password"/>
             </div>
             <div class="margin-top-10 form-actions form-group">
                <button type="submit" class="btn btn-default login-btn" onclick="this.classList.toggle('button--loading')" >Login</button>
             </div>
               </fieldset>
        </form>

        </div>
	  	</div>
  	<div class="col-md-4"></div>
  </div>
  </body>
</html>

