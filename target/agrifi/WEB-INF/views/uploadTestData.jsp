<%@ page contentType="text/html; charset=UTF-8" %>
<%@ page pageEncoding="UTF-8" %>

<%@taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html>
<html>
<head>
  <title>ZedEye Labs</title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">

  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">

  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/sweetalert/2.1.0/sweetalert.min.js"></script>
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/jquery-datetimepicker/2.5.20/jquery.datetimepicker.css" />
  <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-datetimepicker/2.5.20/jquery.datetimepicker.full.js"></script>
  <script src="resources/js/broadcastList.js"></script>

<script type="text/javascript">


    $(document).ready(function(){

    $('#save').click(function(){

            var data = new FormData();
            data.append('name',$("#name").val());
            data.append('emailId',$("#emailId").val());
            data.append('contactNumber',$("#contactNumber").val());
            data.append('roles',$("#roles").val());
            data.append('password',$("#password").val());

            $.ajax({
                url: "saveUser",
                type: "POST",
                contentType: false,
                processData: false,
                cache: false,
                data: data,
                success: function(response) {
                swal("Success!!", "Saved!", "success");
                setTimeout(function(){  location.reload(); }, 2000);
                },
                error: function() {
                }
            });
        });

        $("#uploadExcel").click(function(){
                var formData = new FormData();
                formData.append('file', $('#saveUpload input[type=file]')[0].files[0]);
                console.log(formData);
                    $.ajax({
                        url: "excelProfileUpdate",
                        type: "POST",
                        contentType: false,
                        processData: false,
                        cache: false,
                        data: formData,
                        success: function(response) {
                            swal("Success!!", "Saved!", "success");
                            setTimeout(function(){  location.reload(); }, 2000);
                        },
                        error: function() {
                            swal("Failed!!", "Some thing went wrong!", "error");
                        }
                    });
            });

    });
</script>

  <style>
  /* Note: Try to remove the following lines to see the effect of CSS positioning */
  .affix {
      top: 0;
      width: 100%;
      z-index: 9999 !important;
  }

  .affix + .container-fluid {
      padding-top: 70px;
  }
  .margin-top-10 {
    margin-top:10px;
  }
  .margin-top-5 {
    margin-top:5px;
  }

  .border-box {
      border: 1px solid #ccc;
      padding: 5px;
      margin: 2px;
  }


       .margin-top-15 {
          margin-top : 15px;
         }

       #userList {
        font-family: "Trebuchet MS", Arial, Helvetica, sans-serif;
        border-collapse: collapse;
        width: 100%;
      }

      #userList td, #annList th {
        border: 1px solid #ddd;
        padding: 8px;
      }

      #userList tr:nth-child(even){background-color: #f2f2f2;}

      #userList tr:hover {background-color: #ddd;}

      #userList th {
        padding-top: 12px;
        padding-bottom: 12px;
        text-align: left;
        background-color: #4CAF50;
        color: white;
      }

  </style>
</head>
<body>

<div class="container-fluid" style="background-color:#52AE23;color:#fff;">
    <h2>ZedEye Labs - Admin Panel</h2>
</div>

<nav class="navbar navbar-inverse" data-spy="affix" data-offset-top="197">
    <ul class="nav navbar-nav">
        <li class=""><a href="me">Create User</a></li>
        <li class="active"><a href="uploadTestData">Upload Test Data</a></li>
        <li class=""><a href="done">Upload Control Data</a></li>
        <li class="active" style="position:absolute;right:15px"><a href="logout">Logout</a></li>

    </ul>
</nav>


<div class="border-box">
   <div class="container-fluid" style="">
           <div class="container-fluid">
           <div class="col-md-12">
           <h2>Upload Test Data</h2>
       <div class="container-fluid">
       <form>
           <div class="row" style="padding-bottom: 1%;padding-top: 2%;border: 2px solid #52ae23;margin-left: 0%;margin-right: 0%;">
           <div class="col-md-8">
           <form name="saveUpload"  method="POST" action="saveUpload" >
           <div class="input-group" id="saveUpload" enctype="multipart/form-data">
           <input type="file" class="form-control" placeholder="Choose an excel file" aria-describedby="upload excel file">
           <span class="input-group-btn">
           <button class="btn btn-default" id="uploadExcel" type="button">Upload List</button>
       </span>
       </div>
       </form>

       </div>

</div>

</body>
</html>





