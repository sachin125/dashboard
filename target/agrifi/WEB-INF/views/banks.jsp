<%@ page contentType="text/html; charset=UTF-8" %>
<%@ page pageEncoding="UTF-8" %>
<%@taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html>
<html>
<head>
  <title>AGRIFI LOAN LIST</title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/sweetalert/2.1.0/sweetalert.min.js"></script>
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/jquery-datetimepicker/2.5.20/jquery.datetimepicker.css" />
  <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-datetimepicker/2.5.20/jquery.datetimepicker.full.js"></script>
  <script src="resources/js/broadcastList.js"></script>

<script type="text/javascript">

    $(document).ready(function(){


    $('.tag-btn').click(function(){
     var data = new FormData();
   data.append('loanId',$(this).attr("id"));
            $.ajax({
                url: "accept-by-bank",
                type: "POST",
                contentType: false,
                processData: false,
                cache: false,
                data: data,
                success: function(response) {
                swal("Loan Accepted!!", "Saved!", "success");
                setTimeout(function(){  location.reload(); }, 2000);
                },
                error: function() {
                }
            });
        });
    });


        $(document).ready(function(){
        $('.submitComment').click(function(){
                var data = new FormData();
                data.append('loanId',loanId);
                data.append('addRemark',$('textarea#AddRemark').val());
             $.ajax({
                    url: "add-remark",
                    type: "POST",
                    contentType: false,
                    processData: false,
                    cache: false,
                    data: data,
                    success: function(response) {
                    setTimeout(function(){  location.reload(); }, 1000);
                    },
                    error: function() {
                    }
                });
            });

        });


    $(document).ready(function(){
        $('.tag-btns').click(function(){
               loanId=$(this).attr("id");
            });
        });


 $(document).ready(function(){

        $('#reject').click(function(){

                var data = new FormData();
                data.append('loanId',$(this).attr("id"));
                    $.ajax({
                    url: "reject-by-bank",
                    type: "POST",
                    contentType: false,
                    processData: false,
                    cache: false,
                    data: data,
                    success: function(response) {
                    swal("Loan Rejected!!", "Saved!", "success");
                    setTimeout(function(){  location.reload(); }, 5000);
                    },
                    error: function() {
                    }
                });
            });

        });

           $(document).ready(function(){
                  $("#myInput").on("keyup", function() {
                    var value = $(this).val().toLowerCase();
                    $("#annList tr").filter(function() {
                      $(this).toggle($(this).text().toLowerCase().indexOf(value) > -1)
                    });
                  });
                });

</script>

  <style>
  /* Note: Try to remove the following lines to see the effect of CSS positioning */
  .affix {
      top: 0;
      width: 100%;
      z-index: 9999 !important;
  }

  .affix + .container-fluid {
      padding-top: 70px;
  }
  .margin-top-10 {
    margin-top:10px;
  }
  .margin-top-5 {
    margin-top:5px;
  }

  .border-box {
      border: 1px solid #ccc;
      padding: 5px;
      margin: 2px;
  }


       .margin-top-15 {
          margin-top : 15px;
         }

       #annList {
        font-family: "Trebuchet MS", Arial, Helvetica, sans-serif;
        border-collapse: collapse;
        width: 100%;
      }

      #annList td, #annList th {
        border: 1px solid #ddd;
        padding: 8px;
      }

      #annList tr:nth-child(even){background-color: #f2f2f2;}

      #annList tr:hover {background-color: #ddd;}

      #annList th {
        padding-top: 12px;
        padding-bottom: 12px;
        text-align: left;
        background-color: #4CAF50;
        color: white;
      }
      .tag-btn{
      background-color: #348C31;
       margin-top:10px;
      }
      .tag-btns{
      background-color: #FF0000;
      margin-top:10px;
      }
      .btn-primary{
     margin-top:10px;
     background-color: #348C31;

      }

       .button:active {
         background: #007a63;
       }
           .form-control{
                     margin-top:10px;
                     width:-80px;
                    }
                    .btn-block{
                    margin-top:10px;
                      width:30%;
                     }


     .dropbtn {
       background-color: #3498DB;
       color: white;
       padding: 16px;
       font-size: 16px;
       border: none;
       cursor: pointer;
     }

     .dropbtn:hover, .dropbtn:focus {
       background-color: #2980B9;
     }

     .dropdown {
       position: relative;
       display: inline-block;
     }

     .dropdown-content {
       display: none;
       position: absolute;
       background-color: #f1f1f1;
       min-width: 160px;
       overflow: auto;
       box-shadow: 0px 8px 16px 0px rgba(0,0,0,0.2);
       z-index: 1;
     }

     .dropdown-content a {
       color: black;
       padding: 12px 16px;
       text-decoration: none;
       display: block;
     }


.button:active {
               background: #007a63;
             }
                 .form-control{
                           margin-top:10px;
                           width:-80px;
                          }
                          .btn-block{
                          margin-top:10px;
                            width:30%;
                           }

             .button__text {
               font: bold 20px "Quicksand", san-serif;
               background-color: #348C31;
               transition: all 0.2s;
             }


             }

             .button__allow {
                      font: bold 20px "Quicksand", san-serif;
                      background-color: #ff0040;
                      transition: all 0.2s;
                    }

                    .running .button__allow {

                       background-color: #ff0040;
                    }

             .button--loading::after {
               content: "";
               position: absolute;
               width: 30px;
               height: 30px;
               top: 0;
               left: 0;
               right: 0;
               bottom: 0;
               margin: auto;
                border: 16px solid #0000ff;
               border-radius: 80%;
                border-top: 16px solid #3498db;
               width: 75px;
               height: 75px;
              -webkit-animation: spin 2s linear infinite; /* Safari */
               animation: spin 2s linear infinite;
             }
            @keyframes button-loading-spinner {
                     from {
                       transform: rotate(0turn);
                     }

                     to {
                       transform: rotate(1turn);
                     }
                   }

                    .button--loading .button {
                                          visibility: hidden;
                                          opacity: 0;
                                          background-color:#FF0000;
                                        }

                                        @keyframes spin {
                                          0% { transform: rotate(0deg); }
                                          100% { transform: rotate(360deg); }
                                        }

                    .running::after {
                      content: "";
                      position: absolute;
                      width: 30px;
                      height: 30px;
                      top: 0;
                      left: 0;
                      right: 0;
                      bottom: 0;
                      margin: auto;
                      border: 16px solid #0000ff;
                      border-radius: 80%;
                      border-top: 16px solid #3498db;
                      width: 75px;
                      height: 75px;
                      -webkit-animation: spin 2s linear infinite; /* Safari */
                      animation: spin 2s linear infinite;
                    }
                   @keyframes running-spinner {
                            from {
                              transform: rotate(0turn);
                            }

                            to {
                              transform: rotate(1turn);
                            }
                          }

                           .running .button {
                                                 visibility: hidden;
                                                 opacity: 0;
                                                 background-color:#FF0000;
                                               }

                                               @keyframes spin {
                                                 0% { transform: rotate(0deg); }
                                                 100% { transform: rotate(360deg); }
                                               }




                to {
                  transform: rotate(1turn);
                }
              }

               .button--loading .button {
                                     visibility: hidden;
                                     opacity: 0;
                                     background-color:#FF0000;
                                   }

                                   @keyframes spin {
                                     0% { transform: rotate(0deg); }
                                     100% { transform: rotate(360deg); }
                                   }





  </style>
</head>
<body>

<div class="container-fluid" style="background-color:#52AE23;color:#fff;">
    <h2>AGRIFI LOAN LIST</h2>
</div>

<nav class="navbar navbar-inverse" data-spy="affix" data-offset-top="197">
    <ul class="nav navbar-nav">
                 <li class="active"><a href="get-forwarded-bank-list">Bank List</a></li>
        <li class="active" style="position:absolute;right:15px"><a href="logout">Logout</a></li>

    </ul>

    <div class="col-md-6">
          <input type="text" class="form-control" id="myInput" placeholder="Search.....">
     </div>

  <div class="dropdown">
    <button class="btn btn-secondary dropdown-toggle" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
     Sort By
    </button>
    <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
      <a class="dropdown-item" href="#">Oldest One</a><br>
      <a class="dropdown-item" href="#">Latest One</a>

    </div>
  </div>
</nav>

<div class="border-box">
     <table id="annList" style="width:100%;font-size: 12px;" class="annList">
               <tr>
                  <th>Loan Id</th>
                   <th>Pan no</th>
                   <th>Land Document Link</th>
                    <th>Bank Id</th>
                     <th>Loan Status</th>
                     <th>Loan Type</th>
                      <th>Date</th>
                     <th>User Id</th>
                    <th>Aadhaar no</th>
                     <th>Farm Id</th>
                    <th>GstCertificate Link</th>
                    <th>Internal Status</th>
                     <th>Add remark</th>
                     <th>Current Bank</th>
                     <th>Action</th>

 </tr>
 <c:forEach items="${qualifiedBankList}" var="l">

             <tr>
            <td>
${l.loanId}
</td>

<td>
 ${l.panNo}
</td>




<td>
${l.createdOn.toString().substring(0, l.createdOn.toString().indexOf(' '))}
</td>

<td>
 ${l.panNo}
 </td>

  <td>
 ${l.aadhaarNo}
 </td>

<td>
 <a href="${l.aadhaarFrontLink}" data-target="#${l.loanId}"data-toggle="modal" >Show Image</a>
 <div id="${l.loanId}" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
 <div class="modal-dialog">
 <div class="modal-content">
 <div class="modal-body">
 <img src="${l.aadhaarFrontLink}" class="img-responsive">
</div>
 </div>
 </div>
 </div>
  </td>

 <td>
 <a href="${l.aadhaarBackLink}" data-target="#${l.loanId}"data-toggle="modal" >Show Image</a>
 <div id="${l.loanId}" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog">
<div class="modal-content">
<div class="modal-body">
  <img src="${l.aadhaarBackLink}" class="img-responsive">
   </div>
 </div>
</div>
 </div>
</td>

<td>
       <a href="${l.panLink}" data-target="#${l.loanId}"data-toggle="modal" >Show Image</a>
<div id="${l.loanId}" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
<div class="modal-content">
  <div class="modal-body">
 <img src="${l.panLink}" class="img-responsive">
   </div>
 </div>
 </div>
  </div>
  </td>

 <td>
 <a href="${l.landDocumentLink}" data-target="#${l.loanId}"data-toggle="modal" >Show Image</a>
<div id="${l.loanId}" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
<div class="modal-body">
<img src="${l.landDocumentLink}" class="img-responsive">
 </div>
  </div>
</div>
</div>
 </td>

<td>
<a href="${l.gstCertificateLink}" data-target="#${l.loanId}" data-toggle="modal" >Show Image</a>
 <div id="${l.loanId}" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-body">
<img src="${l.gstCertificateLink}" class="img-responsive">
</div>
</div>
</div>
</div>
</td>

<td>
 ${l.internalStatus}
</td>

<td>
 ${l.currentBank}
</td>
<td>
  <div class="loader accept">
<button type="button" class="button tag-btn" onclick="this.classList.toggle('button--loading')" id="${l.loanId}" data="${l.loanId}">
 <span class="button__text">Accept</span>
 </div>

  <div class="loader reject">
    <button type="button" class="button tag-btns" onclick="this.classList.toggle('running')" id="${l.loanId}" data="${l.loanId}">
   <span class="button__allow">Reject</span>
   </div>

 </td>


 </tr>
 </c:forEach>
 </table>

</div>

<div class="modal fade" id="exampleModal" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
         </div>

      <div class="modal-body">
        <form>
             <div class="form-group">
            <label for="message-text" class="col-form-label" id="myRemark">Add Remarks</label>
            <textarea type="text" class="form-control" id="AddRemark" placeholder="Comment"></textarea>
          </div>
         </form>
      </div>

      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
<button class="btn btn-primary submitComment" id="${l.loanId}" value="${l.loanId}" type="button"  onclick="window.location.reload(true)">Submit</button>
     </div>
    </div>
  </div>
</div>

</body>
</html>