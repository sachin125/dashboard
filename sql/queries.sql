CREATE USER 'agrifi'@'%' IDENTIFIED BY 'ZedEye$123';

GRANT ALL ON *.* TO 'agrifi'@'localhost';

GRANT ALL ON *.* TO 'agrifi'@'%';


create table company
(
   company_id       SERIAL PRIMARY KEY,
   company_name     varchar(256),
   email_id          varchar(256),
   contact_number     varchar(256),
   address             varchar(256),
   pincode             varchar(256),
   created_date         timestamp NOT NULL DEFAULT current_timestamp,
   last_modified_date   timestamp NOT NULL DEFAULT current_timestamp
);


create table users
(
   user_id               SERIAL PRIMARY KEY,
   name                  varchar(256),
   email_id              varchar(256),
   contact_number         varchar(256),
   address                 varchar(256),
   password             varchar(256), 
   company_id                int,
   created_date         timestamp NOT NULL DEFAULT current_timestamp,
   last_modified_date   timestamp NOT NULL DEFAULT current_timestamp,
   enabled                boolean default true
);

alter table users add roles varchar(600);

alter table users add client_id varchar(600);

alter table users add reg_id varchar(600);

alter table users add auth_token varchar(600);

alter table users add auth_token_created_date timestamp;
alter table users add source varchar(600);




create table marketing_engine
(
   id               SERIAL PRIMARY KEY,
   cust_code            varchar(256),
   cust_name                  varchar(256),
   cust_mob              varchar(256),
   location         varchar(256),
   status                 varchar(256),
   basket_inception_90    decimal(65),
   basket_90             decimal(65),
   priority_inception_90             decimal(65),
   priority_90             decimal(65),
   margin_perc             decimal(65),
   fav_week             varchar(256),
   odd_even             varchar(256),
   fav_day             varchar(256),
   fav_time             varchar(256),
   30_Subgroup1             varchar(256),
   90_Subgroup1            varchar(256),
   channel             varchar(256),
   content             varchar(500),
   target_date             varchar(500),
   answered TINYINT(4) NULL DEFAULT '0',
   created_date         timestamp NOT NULL DEFAULT current_timestamp
);