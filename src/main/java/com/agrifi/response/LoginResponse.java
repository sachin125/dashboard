package com.agrifi.response;

import com.agrifi.model.User;
import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.*;

@Builder
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode
public class LoginResponse {

    @JsonInclude(value = JsonInclude.Include.NON_EMPTY)
    private ErrorResponse error;
    @JsonInclude(value = JsonInclude.Include.NON_EMPTY)
    private User user;
    @JsonInclude(value = JsonInclude.Include.NON_EMPTY)
    private String message;


}
