package com.agrifi.dao;

import com.agrifi.model.*;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.transform.Transformers;
import org.hibernate.type.DateType;
import org.hibernate.type.StringType;
import org.hibernate.type.TimestampType;
import org.joda.time.DateTime;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.sql.Statement;
import java.sql.Timestamp;
import java.util.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

@Component
@Transactional("transactionManager")
@Repository("agrifiDao")
public class ZELDaoImpl extends AbstractDao implements ZELDao {

	@Autowired
	private SessionFactory sessionFactory;

	private JdbcTemplate jdbcTemplate;
	// JdbcTemplate setter
	public void setJdbcTemplate(JdbcTemplate jdbcTemplate)
	{
		this.jdbcTemplate = jdbcTemplate;
	}



	public void saveUserCompany(User user) {
		try {
			BCryptPasswordEncoder passwordEncoder = new BCryptPasswordEncoder();
			String hashedPass = passwordEncoder.encode(user.getPassword());
			user.setPassword(hashedPass);
			getSession().saveOrUpdate(user.getCompany());
			user.setCompanyId(user.getCompany().getCompanyId());
			getSession().saveOrUpdate(user);
		} catch(Exception e){
			e.printStackTrace();
		}

	}

	public UserDetails loadUserByUserName(String username) {
		Query query;
		String EMAIL_PATTERN =
				"^[_A-Za-z0-9-\\+]+(\\.[_A-Za-z0-9-]+)*@"
						+ "[A-Za-z0-9-]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$";
		Pattern pattern = Pattern.compile(EMAIL_PATTERN);
		Matcher matcher = pattern.matcher(username);
		if(matcher.matches()){
			query = getSession().createQuery("from User where emailId = :email");
			query.setString("email", username);
		} else {
			query = getSession().createQuery("from User where contactNumber = :contact");
			query.setString("contact", username);
		}
		System.out.println("***** here 1");
		User user = (User) query.list().get(0);
		System.out.println("***** here 2"+user);

		Collection<GrantedAuthority> authorities = new ArrayList<GrantedAuthority>();
		if(user!=null && user.getRoles()!=null && !user.getRoles().isEmpty()){
			for(String role : user.getRoles().split(",")){
				authorities.add(new SimpleGrantedAuthority(role));
			}
		} else {
			authorities.add(new SimpleGrantedAuthority("ROLE_USER"));
		}
		return new org.springframework.security.core.userdetails.User(user.getEmailId(), user.getPassword(), authorities);
	}

	@Override
	public User getUserByEmail(String email) {
		System.out.println("HERE before");
		Query qry = getSession().createQuery("from User u where u.emailId = :email");
		qry.setString("email", email);
		if(qry.list().isEmpty()){
			return null;
		}
		User user = (User) qry.list().get(0);
		System.out.println("HERE after"+user.getName());
		return user;
	}


	@Override
	public BankDetails getByBank(String bankName) {
		System.out.println("HERE before");
		Query qry = getSession().createQuery("from BankDetails b where b.bankName=:bankName");
		qry.setString("bankName", bankName);
		if(qry.list().isEmpty()){
			return null;
		}
		BankDetails bankDetails = (BankDetails) qry.list().get(0);
		System.out.println("HERE after"+bankDetails.getBankName());
		return bankDetails;
	}

	public User getUserById(String id){
		System.out.println("***********");
		Query qry = getSession().createQuery("from User u where u.id = :id");
		qry.setString("id", id);
		User dbUser = null;
		if(!qry.list().isEmpty()){
			dbUser = (User) qry.list().get(0);
		} else {
			return null;
		}
		return dbUser;
	}

	@Override
	public void updateUserCompany(User user) {
		try {
			getSession().saveOrUpdate(user);
		} catch(Exception e){
			e.printStackTrace();
		}
	}

	@Override
	public void updateAuthToken(User user) {
		try {
			getSession().saveOrUpdate(user);
		} catch(Exception e){
			e.printStackTrace();
		}
	}

	@Override
	public User getUserByToken(String strToken) {
		Query qry = getSession().createQuery("from User u where u.authToken = :authToken");
		qry.setString("authToken", strToken);
		User user = (User) qry.list().get(0);
		return user;
	}

	@Override
	public User getUserByEmailOrPhone(String emailId, String contactNumber) {
		Query qry = getSession().createQuery("from User u where u.emailId = :emailId or u.contactNumber = :contactNumber");
		qry.setString("emailId", emailId);
		qry.setString("contactNumber", contactNumber);
		User user = null;
		try {
			user =  (User) qry.list().get(0);
		} catch(Exception e) {
			return user;
		}
		return user;
	}


	@Override
	public  User saveUser(User user) {
		try {
			BCryptPasswordEncoder passwordEncoder = new BCryptPasswordEncoder();
			StringBuilder password = new StringBuilder();
			password.append(user.getPassword());
			String hashedPass = passwordEncoder.encode(password);
			user.setPassword(hashedPass);
			user.setRoles(user.getRoles().trim());
			user.setEmailId(user.getEmailId()); //add email address later here it is a hack

			UUID uniqueToken = UUID.randomUUID();
			String uniqueTokenStr = (user.getContactNumber()+uniqueToken).replaceAll("[-+.^:,]","");
			user.setAuthToken(uniqueTokenStr);
			DateTime dt = new DateTime();
			DateTimeFormatter fmt = DateTimeFormat.forPattern("yyyy-MM-dd HH:mm:ss.S");
			String createdDate = fmt.print(dt);
			Timestamp timestamp = new Timestamp(fmt.parseLocalDateTime(createdDate).toDateTime().getMillis());
			user.setAuthTokenCreatedDate(timestamp);

			getSession().saveOrUpdate(user);

			return getUser(user);

		} catch(Exception e) {
			e.printStackTrace();
			return null;
		}
	}

	@Override
	public List<LoanDetailsNew> getAllLoanDetails() {

		Query qry = getSession().createSQLQuery("select l.loan_id as loanId,k.name as name,k.mobile as contactNumber,l.loan_type loanType,b.bank_name as bankName,l.created_on as createdOn,l.pan_no as panNo,l.aadhaar_no as aadhaarNo,\n" +
				"k.aadhaar_front_link as aadhaarFrontLink,k.aadhaar_back_link as aadhaarBackLink,k.pan_link as panLink,\n"+
				"l.land_document_link as landDocumentLink,l.gst_certificate_link as gstCertificate,l.internal_status as internalStatus,l.current_bank as currentBank from loan_details_new l\n"+
				"inner join khata_user k on l.user_id=k.agrifi_id\n " +
				"inner join bank_details b on l.bank_id=b.bank_id\n"+
				"where internal_status='Applied'")
				.addScalar("loanId", new StringType())
				.addScalar("name", new StringType())
				.addScalar("contactNumber",new StringType())
				.addScalar("loanType", new StringType())
				.addScalar("bankName", new StringType())
				.addScalar("createdOn", new TimestampType())
				.addScalar("panNo", new StringType())
				.addScalar("aadhaarNo", new StringType())
				.addScalar("aadhaarFrontLink", new StringType())
				.addScalar("aadhaarBackLink", new StringType())
				.addScalar("panLink", new StringType())
				.addScalar("landDocumentLink", new StringType())
				.addScalar("gstCertificate", new StringType())
				.addScalar("internalStatus", new StringType())
				.addScalar("currentBank",new StringType())
				.setResultTransformer(Transformers.aliasToBean(LoanDetailsNew.class));

		List<LoanDetailsNew> loanDetailsNews =  qry.list();
		return loanDetailsNews;
}

	@Override
	public void markCallDone(String id) {

		Query qry = getSession().createQuery("from MarketingEngine m where m.id = "+id+" ");
		MarketingEngine marketingEngine = (MarketingEngine) qry.list().get(0);

		marketingEngine.setAnswered(true);
		marketingEngine.setTargetDate(new DateTime().toString());
		getSession().saveOrUpdate(marketingEngine);

	}

	@Override
	public List<User> getUserList() {
		Query qry = getSession().createQuery("from User");
		List<User> users = qry.list();
		return users;
	}

	@Override
	public List<LoanDetailsNew> getQualifiedList() {
		Query qry = getSession().createSQLQuery("select l.loan_id as loanId,k.name as name,l.loan_type loanType,b.bank_name as bankName,l.created_on as createdOn,l.pan_no as panNo,l.aadhaar_no as aadhaarNo,\n" +
						"k.aadhaar_front_link as aadhaarFrontLink,k.aadhaar_back_link as aadhaarBackLink,k.pan_link as panLink,\n"+
						"l.land_document_link as landDocumentLink,l.gst_certificate_link as gstCertificate,l.internal_status as internalStatus from loan_details_new l\n"+
						"inner join khata_user k on l.user_id=k.agrifi_id\n " +
						"inner join bank_details b on l.bank_id=b.bank_id\n"+
						"where internal_status='Qualified'")
				.addScalar("loanId", new StringType())
				.addScalar("name", new StringType())
				.addScalar("loanType", new StringType())
				.addScalar("bankName", new StringType())
				.addScalar("createdOn", new TimestampType())
				.addScalar("panNo", new StringType())
				.addScalar("aadhaarNo", new StringType())
				.addScalar("aadhaarFrontLink", new StringType())
				.addScalar("aadhaarBackLink", new StringType())
				.addScalar("panLink", new StringType())
				.addScalar("landDocumentLink", new StringType())
				.addScalar("gstCertificate", new StringType())
				.addScalar("internalStatus", new StringType())
				.setResultTransformer(Transformers.aliasToBean(LoanDetailsNew.class));

		List<LoanDetailsNew> loanDetailsNews =  qry.list();
		return loanDetailsNews;
	}
	@Override
	public List<LoanDetailsNew> getForwardedBankList(){
		Query qry = getSession().createSQLQuery("select l.loan_id as loanId,k.name as name,l.loan_type loanType,b.bank_name as bankName,l.created_on as createdOn,l.pan_no as panNo,l.aadhaar_no as aadhaarNo,\n" +
						"k.aadhaar_front_link as aadhaarFrontLink,k.aadhaar_back_link as aadhaarBackLink,k.pan_link as panLink,\n"+
						"l.land_document_link as landDocumentLink,l.gst_certificate_link as gstCertificate,l.internal_status as internalStatus,l.current_bank as currentBank,l.add_remark as addRemark from loan_details_new l\n"+
						"inner join khata_user k on l.user_id=k.agrifi_id\n " +
						"inner join bank_details b on l.bank_id=b.bank_id\n"+
						"where internal_status='Forwarded to Bank' or internal_status='Accepted By Bank' or internal_status='Rejected By Bank'")
				.addScalar("loanId", new StringType())
				.addScalar("name", new StringType())
				.addScalar("loanType", new StringType())
				.addScalar("bankName", new StringType())
				.addScalar("createdOn", new TimestampType())
				.addScalar("panNo", new StringType())
				.addScalar("aadhaarNo", new StringType())
				.addScalar("aadhaarFrontLink", new StringType())
				.addScalar("aadhaarBackLink", new StringType())
				.addScalar("panLink", new StringType())
				.addScalar("landDocumentLink", new StringType())
				.addScalar("gstCertificate", new StringType())
				.addScalar("internalStatus", new StringType())
				.addScalar("currentBank", new StringType())
				.addScalar("addRemark",new StringType())
				.setResultTransformer(Transformers.aliasToBean(LoanDetailsNew.class));

		List<LoanDetailsNew> loanDetailsNews =  qry.list();
		return loanDetailsNews;
	}


	@Override
	public int updateInternalStatus(String loanId,String internalStatus) {
		Query query=getSession().createQuery("update LoanDetails set internalStatus=:internalStatus where loanId=:loanId");
		query.setParameter("loanId",loanId);
		query.setParameter("internalStatus",internalStatus);
		int i=query.executeUpdate();
		return i;
	}


	@Override
	public List<LoanDetails> rejectInternalStatus(String loanId,String internalStatus) {
		Query query=getSession().createQuery("from LoanDetails l where l.loanId=:loanId");
		query.setString("loanId",loanId.toString());

		List<LoanDetails> details=query.list();
		LoanDetails ld = details.get(0);
		ld.setInternalStatus("Rejected");
		getSession().saveOrUpdate(ld);
		return details;
	}

	@Override
	public int updateInternalBankStatus(String loanId, String internalStatus,String bankId) {
		Query query=getSession().createQuery("update LoanDetails set internal_status=:internalStatus,current_bank=:bankId where loanId=:loanId");
		query.setParameter("loanId",loanId);
		query.setParameter("internalStatus",internalStatus);
		query.setParameter("bankId",bankId);

		int i=query.executeUpdate();
		return i;
	}

	@Override
	public List<LoanDetails> acceptByBank(String loanId, String internalStatus) {
		Query query=getSession().createQuery("from LoanDetails l where l.loanId=:loanId");
		query.setString("loanId",loanId.toString());
		List<LoanDetails> details=query.list();
		LoanDetails ld = details.get(0);
		ld.setInternalStatus("Accepted By Bank");
		getSession().saveOrUpdate(ld);
		return details;
	}

	@Override
	public List<LoanDetails> acceptByICICIBank(String loanId, String internalStatus) {
		Query query=getSession().createQuery("from LoanDetails l where l.loanId=:loanId");
		query.setString("loanId",loanId.toString());
		List<LoanDetails> details=query.list();
		LoanDetails ld = details.get(0);
		ld.setInternalStatus("Accepted By ICICI Bank");
		getSession().saveOrUpdate(ld);
		return details;
	}


	@Override
	public List<LoanDetails> rejectByBank(String loanId, String internalStatus) {
		Query query=getSession().createQuery("from LoanDetails l where l.loanId=:loanId");
		query.setString("loanId",loanId.toString());
		List<LoanDetails> details=query.list();
		LoanDetails ld = details.get(0);
		ld.setInternalStatus(internalStatus);
		getSession().saveOrUpdate(ld);
		return details;
	}


	@Override
	public int addRemark(String loanId,String addRemark) {
		Query query=getSession().createQuery("update LoanDetails set addRemark=:addRemark where loanId=:loanId");
		query.setParameter("loanId",loanId);
		query.setParameter("addRemark",addRemark);
		int i=query.executeUpdate();
		return i;
		}



	@Override
	public List<BankDetails> getBankList(){
		Query query=getSession().createQuery("from bank_details");
		List<BankDetails> details=query.list();
		return details;
	}


	@Override
	public BankDetails getBankById(String bankId){
		Query query=getSession().createQuery("from bank_details b where b.bank_id="+bankId+"");
		query.setString("id", bankId);
		BankDetails dbUser = null;
		if(!query.list().isEmpty()){
			dbUser = (BankDetails) query.list().get(0);
		} else {
			return null;
		}
		return dbUser;

	}

	@Override
	public List<MarketingEngine> getMEDataByRoleStatus1(String roles, boolean b) {
		Query qry = getSession().createQuery("from MarketingEngine m where m.location in ('"+roles+"') and m.answered = "+b+" ");
		List<MarketingEngine> marketingEngineList = (List<MarketingEngine>) qry.list();
		return marketingEngineList;
	}

	private User getUser(User user) {
		Query qry = getSession().createQuery("from User u where u.contactNumber = :contactNumber");
		qry.setString("contactNumber", user.getContactNumber());
		User newUser = (User) qry.list().get(0);
		return newUser;
	}

}