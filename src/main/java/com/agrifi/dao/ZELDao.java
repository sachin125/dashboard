package com.agrifi.dao;

import com.agrifi.model.*;
import org.springframework.security.core.userdetails.UserDetails;

import java.util.List;

public interface ZELDao {

	UserDetails loadUserByUserName(String username);

	User getUserByEmail(String email);

	BankDetails getByBank(String bankName);

	User getUserById(String id);

	void updateUserCompany(User user);

	void updateAuthToken(User user);

	User getUserByToken(String strToken);

	User getUserByEmailOrPhone(String emailId, String contactNumber);

	User saveUser(User user);

	List<LoanDetailsNew> getAllLoanDetails();

	void markCallDone(String id);

	List<User> getUserList();

	List<LoanDetailsNew> getQualifiedList();

	List<LoanDetailsNew> getForwardedBankList();

	int updateInternalStatus(String loanId,String internalStatus);

	List<LoanDetails> rejectInternalStatus(String loanId,String internalStatus);

int updateInternalBankStatus(String loanId,String internalStatus, String bankId);

	List<LoanDetails> acceptByBank(String loanId,String internalStatus);

	List<LoanDetails> acceptByICICIBank(String loanId,String internalStatus);

	List<LoanDetails> rejectByBank(String loanId,String internalStatus);

int addRemark(String loanId,String addRemark);

	List<BankDetails> getBankList();

	BankDetails getBankById(String bankId);


	List<MarketingEngine> getMEDataByRoleStatus1(String roles, boolean b);
}