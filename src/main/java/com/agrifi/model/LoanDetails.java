package com.agrifi.model;

import javax.persistence.*;
import java.sql.Timestamp;


@Entity
@Table(name="loan_details_new")
public class LoanDetails {


    @Id
    @Column(name = "loan_id")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private  String loanId;
    @Column(name = "aadhaar_no")
    private  String aadhaarNo;
    @Column(name = "user_id")
    private  String userId;
    @Column(name = "bank_id")
    private  String bankId;

    @Column(name = "farm_id")
    private  String farmId;
    @Column(name = "pan_no")
    private String panNo;
    @Column(name = "loan_status")
    private String loanStatus;
    @Column(name = "loan_type")
    private  String loanType;
    @Column(name = "land_document_link")
    private String landDocumentLink;
    @Column(name = "gst_certificate_link")
    private String gstCertificateLink;

    @Column(name = "add_remark")
    private String addRemark;
    @Column(name = "internal_status")
    private String internalStatus;

    @Column(name = "current_bank")
    private String currentBank;

    @Column(name = "created_on")
    private Timestamp createdOn;
    @Column(name = "updated_on")
    private Timestamp updatedOn;



    public String getAddRemark() {
        return addRemark;
    }

    public void setAddRemark(String addRemark) {
        this.addRemark = addRemark;
    }

    public String getLoanId() {
        return loanId;
    }

    public void setLoanId(String loanId) {
        this.loanId = loanId;
    }

    public String getAadhaarNo() {
        return aadhaarNo;
    }

    public void setAadhaarNo(String aadhaarNo) {
        this.aadhaarNo = aadhaarNo;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getBankId() {
        return bankId;
    }

    public void setBankId(String bankId) {
        this.bankId = bankId;
    }

    public String getFarmId() {
        return farmId;
    }

    public void setFarmId(String farmId) {
        this.farmId = farmId;
    }

    public String getPanNo() {
        return panNo;
    }

    public void setPanNo(String panNo) {
        this.panNo = panNo;
    }

    public String getLoanStatus() {
        return loanStatus;
    }

    public void setLoanStatus(String loanStatus) {
        this.loanStatus = loanStatus;
    }

    public String getLoanType() {
        return loanType;
    }

    public void setLoanType(String loanType) {
        this.loanType = loanType;
    }

    public String getLandDocumentLink() {
        return landDocumentLink;
    }

    public void setLandDocumentLink(String landDocumentLink) {
        this.landDocumentLink = landDocumentLink;
    }


    public String getGstCertificateLink() {
        return gstCertificateLink;
    }

    public void setGstCertificateLink(String gstCertificateLink) {
        this.gstCertificateLink = gstCertificateLink;
    }

    public String getInternalStatus() {
        return internalStatus;
    }


    public String getCurrentBank() {
        return currentBank;
    }



    public void setInternalStatus(String internalStatus) {
        this.internalStatus = internalStatus;
    }

    public void setCurrentBank(String currentBank) {
        this.currentBank = currentBank;
    }

    public Timestamp getCreatedOn() {
        return createdOn;
    }

    public void setCreatedOn(Timestamp createdOn) {
        this.createdOn = createdOn;
    }

    public Timestamp getUpdatedOn() {
        return updatedOn;
    }

    public void setUpdatedOn(Timestamp updatedOn) {
        this.updatedOn = updatedOn;
    }
}