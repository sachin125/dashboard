package com.agrifi.model;

import javax.persistence.*;

@Entity
@Table(name="marketing_engine")
public class MarketingEngine {
    @Id
    @Column(name = "id")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    @Column(name = "cust_code")
    private String custCode;

    @Column(name = "cust_name")
    private String custName;

    @Column(name = "cust_mob")
    private String custMob;

    @Column(name = "location")
    private String location;

    @Column(name = "status")
    private String status;

    @Column(name = "basket_inception_90")
    private Double basketInception90;

    @Column(name = "basket_90")
    private Double basket90;

    @Column(name = "priority_inception_90")
    private Double priorityInception90;

    @Column(name = "priority_90")
    private Double priority90;

    @Column(name = "margin_perc")
    private Double marginPerc;

    @Column(name = "fav_week")
    private String favWeek;
    @Column(name = "odd_even")
    private String oddEven;
    @Column(name = "fav_day")
    private String favDay;

    @Column(name = "fav_time")
    private String favTime;

    @Column(name = "Subgroup1")
    private String subgroup1_30;

    @Column(name = "Subgroup2")
    private String subgroup1_90;
    @Column(name = "channel")
    private String channel;
    @Column(name = "content")
    private String content;
    @Column(name = "answered")
    private Boolean answered;

    @Column(name = "target_date")
    private String targetDate;

    @Column(name = "created_date")
    private String createdDate;


    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getCustCode() {
        return custCode;
    }

    public void setCustCode(String custCode) {
        this.custCode = custCode;
    }

    public String getCustName() {
        return custName;
    }

    public void setCustName(String custName) {
        this.custName = custName;
    }

    public String getCustMob() {
        return custMob;
    }

    public void setCustMob(String custMob) {
        this.custMob = custMob;
    }

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public Double getBasketInception90() {
        return basketInception90;
    }

    public void setBasketInception90(Double basketInception90) {
        this.basketInception90 = basketInception90;
    }

    public Double getBasket90() {
        return basket90;
    }

    public void setBasket90(Double basket90) {
        this.basket90 = basket90;
    }

    public Double getPriorityInception90() {
        return priorityInception90;
    }

    public void setPriorityInception90(Double priorityInception90) {
        this.priorityInception90 = priorityInception90;
    }

    public Double getPriority90() {
        return priority90;
    }

    public void setPriority90(Double priority90) {
        this.priority90 = priority90;
    }

    public Double getMarginPerc() {
        return marginPerc;
    }

    public void setMarginPerc(Double marginPerc) {
        this.marginPerc = marginPerc;
    }

    public String getFavWeek() {
        return favWeek;
    }

    public void setFavWeek(String favWeek) {
        this.favWeek = favWeek;
    }

    public String getOddEven() {
        return oddEven;
    }

    public void setOddEven(String oddEven) {
        this.oddEven = oddEven;
    }

    public String getFavDay() {
        return favDay;
    }

    public void setFavDay(String favDay) {
        this.favDay = favDay;
    }

    public String getFavTime() {
        return favTime;
    }

    public void setFavTime(String favTime) {
        this.favTime = favTime;
    }

    public String getSubgroup1_30() {
        return subgroup1_30;
    }

    public void setSubgroup1_30(String subgroup1_30) {
        this.subgroup1_30 = subgroup1_30;
    }

    public String getSubgroup1_90() {
        return subgroup1_90;
    }

    public void setSubgroup1_90(String subgroup1_90) {
        this.subgroup1_90 = subgroup1_90;
    }

    public String getChannel() {
        return channel;
    }

    public void setChannel(String channel) {
        this.channel = channel;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public Boolean getAnswered() {
        return answered;
    }

    public void setAnswered(Boolean answered) {
        this.answered = answered;
    }

    public String getTargetDate() {
        return targetDate;
    }

    public void setTargetDate(String targetDate) {
        this.targetDate = targetDate;
    }

    public String getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(String createdDate) {
        this.createdDate = createdDate;
    }
}

