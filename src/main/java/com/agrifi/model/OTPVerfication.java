package com.agrifi.model;

import javax.persistence.*;


@Entity
@Table(name="otp_verfication")
public class OTPVerfication {
		
		@Id
		@Column(name = "otp_verfication_id", nullable = false)
		@GeneratedValue(strategy = GenerationType.IDENTITY)
		private int otpVerficationId;

		@Column(name = "contact_number", nullable = true)
		private String contactNumber;
		
		@Column(name = "email_id", nullable = true)
		private String emailId;
		
		@Column(name = "otp_code", nullable = true)
		private int otpCode;
		
		@Column(name ="created_date")
		private String createdDate;

		public int getOtpVerficationId() {
			return otpVerficationId;
		}

		public void setOtpVerficationId(int otpVerficationId) {
			this.otpVerficationId = otpVerficationId;
		}

		public String getContactNumber() {
			return contactNumber;
		}

		public void setContactNumber(String contactNumber) {
			this.contactNumber = contactNumber;
		}

		public String getEmailId() {
			return emailId;
		}

		public void setEmailId(String emailId) {
			this.emailId = emailId;
		}

		public int getOtpCode() {
			return otpCode;
		}

		public void setOtpCode(int otpCode) {
			this.otpCode = otpCode;
		}

		public String getCreatedDate() {
			return createdDate;
		}

		public void setCreatedDate(String createdDate) {
			this.createdDate = createdDate;
		}
		
}
