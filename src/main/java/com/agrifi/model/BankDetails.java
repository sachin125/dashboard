
package com.agrifi.model;

import javax.persistence.*;

@Entity
@Table(name="bank_details")
public class BankDetails {


    @Id
    @Column(name = "bank_id")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private String bankId;

    @Column(name = "bank_name")
    private String bankName;

//    public LoanDetails getDetails() {
//        return details;
//    }
//
//    public void setDetails(LoanDetails details) {
//        this.details = details;
//    }

//    @OneToOne
//    @JoinColumn(name = "bank")
//    private LoanDetails details;
//
//    public LoanDetails getDetails() {
//        return details;
//    }

//    public void setDetails(LoanDetails details) {
//        this.details = details;
//    }

    public String getBankId() {
        return bankId;
    }

    public void setBankId(String bankId) {
        this.bankId = bankId;
    }

    public String getBankName() {
        return bankName;
    }

    public void setBankName(String bankName) {
        this.bankName = bankName;
    }


}