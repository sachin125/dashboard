package com.agrifi.model;


import javax.persistence.*;


@Entity
@Table(name="khata_user")
public class KhataUser {
    @Id
    @Column(name = "id", nullable = false)
    private Long id;

    @Column(name = "agrifi_id")
    private String agrifiId;
    @Column(name = "name")
    private String name;


    @Column(name = "aadhaar_front_link")
    private String aadhaarFrontLink;
    @Column(name = "aadhaar_back_link")
    private String aadhaarBackLink;
    @Column(name = "pan_link")
    private String panLink;


//    @OneToOne
//    private LoanDetails details;
//
//    public LoanDetails getDetails() {
//        return details;
//    }
//
//    public void setDetails(LoanDetails details) {
//        this.details = details;
//    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getAgrifiId() {
        return agrifiId;
    }

    public void setAgrifiId(String agrifiId) {
        this.agrifiId = agrifiId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAadhaarFrontLink() {
        return aadhaarFrontLink;
    }

    public void setAadhaarFrontLink(String aadhaarFrontLink) {
        this.aadhaarFrontLink = aadhaarFrontLink;
    }

    public String getAadhaarBackLink() {
        return aadhaarBackLink;
    }

    public void setAadhaarBackLink(String aadhaarBackLink) {
        this.aadhaarBackLink = aadhaarBackLink;
    }

    public String getPanLink() {
        return panLink;
    }

    public void setPanLink(String panLink) {
        this.panLink = panLink;
    }
}
