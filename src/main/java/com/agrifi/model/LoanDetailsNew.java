package com.agrifi.model;



import java.sql.Timestamp;


public class LoanDetailsNew  {


private  String loanId;

    private  String aadhaarNo;

    private  String userId;

    private  String bankId;

    private  String farmId;

    private String panNo;

    private String loanStatus;

    private  String loanType;

    private String landDocumentLink;

    private String gstCertificate;
    private String name;
    private String bankName;
    private String aadhaarFrontLink;
    private String aadhaarBackLink;
    private String panLink;

    private String addRemark;

    private String internalStatus;

    private String currentBank;

    private Timestamp createdOn;

    public String getGstCertificate() {
        return gstCertificate;
    }

    public void setGstCertificate(String gstCertificate) {
        this.gstCertificate = gstCertificate;
    }

    public String getCurrentBank() {
        return currentBank;
    }

    public void setCurrentBank(String currentBank) {
        this.currentBank = currentBank;
    }

    private String contactNumber;

    public String getContactNumber() {
        return contactNumber;
    }

    public void setContactNumber(String contactNumber) {
        this.contactNumber = contactNumber;
    }

    public String getLoanId() {
        return loanId;
    }

    public void setLoanId(String loanId) {
        this.loanId = loanId;
    }

    public String getAadhaarNo() {
        return aadhaarNo;
    }

    public void setAadhaarNo(String aadhaarNo) {
        this.aadhaarNo = aadhaarNo;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getBankId() {
        return bankId;
    }

    public void setBankId(String bankId) {
        this.bankId = bankId;
    }

    public String getFarmId() {
        return farmId;
    }

    public void setFarmId(String farmId) {
        this.farmId = farmId;
    }

    public String getPanNo() {
        return panNo;
    }

    public void setPanNo(String panNo) {
        this.panNo = panNo;
    }

    public String getLoanStatus() {
        return loanStatus;
    }

    public void setLoanStatus(String loanStatus) {
        this.loanStatus = loanStatus;
    }

    public String getLoanType() {
        return loanType;
    }

    public void setLoanType(String loanType) {
        this.loanType = loanType;
    }

    public String getLandDocumentLink() {
        return landDocumentLink;
    }

    public void setLandDocumentLink(String landDocumentLink) {
        this.landDocumentLink = landDocumentLink;
    }

    public String getGstCertificateLink() {
        return gstCertificate;
    }

    public void setGstCertificateLink(String gstCertificateLink) {
        this.gstCertificate= gstCertificateLink;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getBankName() {
        return bankName;
    }

    public void setBankName(String bankName) {
        this.bankName = bankName;
    }

    public String getAadhaarFrontLink() {
        return aadhaarFrontLink;
    }

    public void setAadhaarFrontLink(String aadhaarFrontLink) {
        this.aadhaarFrontLink = aadhaarFrontLink;
    }

    public String getAadhaarBackLink() {
        return aadhaarBackLink;
    }

    public void setAadhaarBackLink(String aadhaarBackLink) {
        this.aadhaarBackLink = aadhaarBackLink;
    }

    public String getPanLink() {
        return panLink;
    }

    public void setPanLink(String panLink) {
        this.panLink = panLink;
    }

    public String getAddRemark() {
        return addRemark;
    }

    public void setAddRemark(String addRemark) {
        this.addRemark = addRemark;
    }

    public String getInternalStatus() {
        return internalStatus;
    }

    public void setInternalStatus(String internalStatus) {
        this.internalStatus = internalStatus;
    }

    public Timestamp getCreatedOn() {
        return createdOn;
    }

    public void setCreatedOn(Timestamp createdOn) {
        this.createdOn = createdOn;
    }

}
