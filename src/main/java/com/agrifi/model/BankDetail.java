package com.agrifi.model;

import javax.persistence.*;
import java.sql.Timestamp;



public class BankDetail {

    private  String loanId;
    private String addRemark;


    //    @OneToOne(cascade = CascadeType.ALL)
//    @JoinColumn(name = "bank_id")
//    private BankDetails details;
//
//
//    @OneToOne(cascade = CascadeType.ALL)
//    @JoinColumn(name = "agrifi_id")
//    private KhataUser user;
//
//    public BankDetails getDetails() {
//        return details;
//    }
//
//    public void setDetails(BankDetails details) {
//        this.details = details;
//    }
//
//    public KhataUser getUser() {
//        return user;
//    }
//
//    public void setUser(KhataUser user) {
//        this.user = user;
//    }
    public String getAddRemark() {
        return addRemark;
    }

    public void setAddRemark(String addRemark) {
        this.addRemark = addRemark;
    }

    public String getLoanId() {
        return loanId;
    }

    public void setLoanId(String loanId) {
        this.loanId = loanId;
    }

}