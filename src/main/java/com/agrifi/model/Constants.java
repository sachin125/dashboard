package com.agrifi.model;

public interface Constants {
final String success = "SUCCESS";
final String error = "ERROR";
final String someError = "Something Went Wrong";
final String userExists = "User Exists";
final String ASK_PRICE = "ASK_PRICE";
final String NO_PRODUCTS = "No data available for the selection!";
final String ALREADY_EXISTS = "The data for current selection already exists!";
final String UPDATED = "We have updated the values!";
final String PUBLISHED = "Published Successfully!";
final String ALL = "ALL";
final String NEW = "NEW";
final int CREATED = 0;
final int SENT = 1;
final int DELIVERED = 2;
final int READ = 3;

final int PAGINATION_SIZE = 10;


final String APIKEY = "74573b70abf5a03bd04a72cb30a43b4b";

final int requestsPerMinute = 150;
final String exotelApiKey = "d5b89078e271b6130a04c30da2b869a4525c3b0c";
final String exotelNumber = "07949122865";
final String exotelSid = "precisionag1";
final String pushCallAppId = "216313";
final String IVRAppId = "229748";
final String pushCallback = "http://3.217.152.184:8080/agrifi/callback?APIKEY=74573b70abf5a03bd04a72cb30a43b4b";
final String evening = "20:00";
final String morning = "08:00";

final boolean allowPushCalls = true;

final String error_not_registered = "" ; //We should insert a link to some recording saying "your number is not registered into the system, do X to sign up"
final String error_message = ""; //Message to play to caller if there's a server error

final String answerCallback = "http://3.217.152.184:8080/agrifi/answerCallback?APIKEY=74573b70abf5a03bd04a72cb30a43b4b";

final String answerAppId = "230849";

final String pushCallTemplateId = "230978";

}
