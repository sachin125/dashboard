package com.agrifi.configuration;

import com.agrifi.model.Constants;
import com.agrifi.model.User;
import com.agrifi.service.ZELServices;
import org.joda.time.DateTime;
import org.joda.time.Minutes;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.web.authentication.WebAuthenticationDetails;
import org.springframework.stereotype.Component;
import org.springframework.web.context.support.SpringBeanAutowiringSupport;
import org.springframework.web.filter.GenericFilterBean;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

@Component
@ComponentScan(basePackages = {"com.agrifi","com.agrifi.service"})
public class AuthenticationTokenProcessingFilter extends GenericFilterBean {
	
	
	@Autowired
	@Qualifier("ZELServices")
	private ZELServices service;

    public void doFilter(ServletRequest request, ServletResponse response,
            FilterChain chain) throws IOException, ServletException {
    	
    	SpringBeanAutowiringSupport.processInjectionBasedOnCurrentContext(this);
    	
        @SuppressWarnings("unchecked")
        Map<String, String[]> parms = request.getParameterMap();

        if (parms.containsKey("token")) {
        	try {
        		String strToken = parms.get("token")[0]; // grab the first "token" parameter
            	
            	User user = service.getUserByToken(strToken);
            	System.out.println("Token: " + strToken);
            	
            	DateTime dt = new DateTime();
    			DateTimeFormatter fmt = DateTimeFormat.forPattern("yyyy-MM-dd HH:mm:ss.S");
    			DateTime createdDate = fmt.parseDateTime(user.getAuthTokenCreatedDate().toString());
    			Minutes mins = Minutes.minutesBetween(createdDate, dt);
    			
            	
                if (user != null) { // && mins.getMinutes() <= 30
                    System.out.println("valid token found");
                    
                    List<GrantedAuthority> authorities = new ArrayList<GrantedAuthority>();
                    authorities.add(new SimpleGrantedAuthority("ROLE_ADMIN"));

                    UsernamePasswordAuthenticationToken token = new UsernamePasswordAuthenticationToken(user.getEmailId(), user.getPassword());
                    token.setDetails(new WebAuthenticationDetails((HttpServletRequest) request));
                    Authentication authentication = new UsernamePasswordAuthenticationToken(user.getEmailId(), user.getPassword(), authorities); //this.authenticationProvider.authenticate(token);
                    
                    SecurityContextHolder.getContext().setAuthentication(authentication);
                }else{
                    System.out.println("invalid token");
                }
        	} catch(Exception e) {
        		 System.out.println("Invalid Token or Something Went Wrong "+e.getMessage());
        	}
        } else {
            //System.out.println("no token found");
        }
        
        if(parms.containsKey("APIKEY")){
        	String str = parms.get("APIKEY")[0];
        	
        	if(str.equals(Constants.APIKEY)) {
        		List<GrantedAuthority> authorities = new ArrayList<GrantedAuthority>();
                authorities.add(new SimpleGrantedAuthority("ROLE_APIKEYUSER"));

                UsernamePasswordAuthenticationToken token = new UsernamePasswordAuthenticationToken("APIUSER", "APIUSER");
                token.setDetails(new WebAuthenticationDetails((HttpServletRequest) request));
                Authentication authentication = new UsernamePasswordAuthenticationToken("APIUSER", "APIUSER", authorities); //this.authenticationProvider.authenticate(token);
                
                SecurityContextHolder.getContext().setAuthentication(authentication);
                System.out.println("Authenticated API KEY!");
        	} else {
        		System.out.println("Wrong API KEY!");
        	}
        }
        // continue thru the filter chain
        chain.doFilter(request, response);
    }
}