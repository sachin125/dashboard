package com.agrifi.configuration;

import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.web.access.channel.ChannelProcessingFilter;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;
import org.springframework.stereotype.Component;

@Component
@Configuration
@EnableWebSecurity
public class SecurityConfig extends WebSecurityConfigurerAdapter {

	@Autowired
	DataSource dataSource;

	@Autowired
	private UserDetailsService userDetailsService;

    @Autowired
    public void configureGlobal(AuthenticationManagerBuilder auth) throws Exception {
		auth.userDetailsService(userDetailsService).passwordEncoder(passwordEncoder());
   	}

	@Override
	protected void configure(HttpSecurity http) throws Exception {
		http
				.csrf().disable()
//          .addFilterBefore(new CORSFilter(), ChannelProcessingFilter.class)
//          .addFilterBefore(new AuthenticationTokenProcessingFilter(),UsernamePasswordAuthenticationFilter.class)
				.authorizeRequests()
				.antMatchers("/resources/**").permitAll()
				.antMatchers("/signUp").permitAll()
				.antMatchers("/schedule").permitAll()
				.antMatchers("/pushCalls").permitAll()
				.antMatchers("/saveBook**").permitAll()
				.antMatchers("/saveUser**").permitAll()
				.antMatchers("/saveUserCompanyJSON").permitAll()
				.antMatchers("/loginJSON").permitAll()
				.antMatchers("/login").permitAll()
				.antMatchers("/pushMessage**").permitAll()
				.antMatchers("/getUserByEmailOrPhone**").permitAll()
				.antMatchers("/audioToPlay**").permitAll()
				.antMatchers("/getUsers**").permitAll()
				.antMatchers("/loggedout**").permitAll()
				.antMatchers("/sendDailyStatusEmail**").permitAll()
				.antMatchers("/getVerificationCode/**").permitAll()
				.antMatchers("/verifyOTP/**").permitAll()
				.antMatchers("/admin**").access("hasRole('BANK')")
				.anyRequest().authenticated()
				.and()
				.formLogin().loginPage("/login").permitAll().failureUrl("/login#error").permitAll()
				.and().exceptionHandling().accessDeniedPage("/accessDenied");
		http.logout()
				.logoutUrl("/logout")
				.logoutSuccessUrl("/loggedout");
	}









	@Bean
	public PasswordEncoder passwordEncoder(){
		PasswordEncoder encoder = new BCryptPasswordEncoder();
		return encoder;
	}

	@Bean(name="authenticationManager")
	@Override
	public AuthenticationManager authenticationManagerBean() throws Exception {
		return super.authenticationManagerBean();
	}

}
