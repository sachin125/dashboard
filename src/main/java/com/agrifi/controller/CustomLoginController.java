package com.agrifi.controller;

import com.agrifi.model.User;
import com.agrifi.service.ZELServices;
import org.joda.time.DateTime;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.http.MediaType;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;

import java.util.UUID;

@EnableWebMvc
@RequestMapping("/")
@ComponentScan("com.zel")
public class CustomLoginController {

	@Autowired
	@Qualifier("authenticationManager")
	AuthenticationManager authenticationManager;

	@Autowired
	@Qualifier("ZELServices")
	ZELServices service;

	@Autowired
	private UserDetailsService userDetailsService;

	@RequestMapping(value={"/loginJSON"},method = RequestMethod.POST,produces = {"application/json"}, consumes = {MediaType.APPLICATION_JSON_VALUE})
	public @ResponseBody LoginStatus mylogin(@RequestParam("username") String username,
											 @RequestParam("password") String password, @RequestParam(value="regId",required = false) String regId) {
		UsernamePasswordAuthenticationToken token = new UsernamePasswordAuthenticationToken(username, password);
		org.springframework.security.core.userdetails.User details = (org.springframework.security.core.userdetails.User) userDetailsService.loadUserByUsername(username);
		token.setDetails(details);
		try {
			Authentication auth = authenticationManager.authenticate(token);
			SecurityContextHolder.getContext().setAuthentication(auth);
			User user = service.getUserByEmail(auth.getName());
			if(auth.isAuthenticated()){
				UUID uniqueToken = UUID.randomUUID();
				String uniqueTokenStr = (auth.getName()+uniqueToken).replaceAll("[-+.^:,]","");
				user.setAuthToken(uniqueTokenStr);
				DateTime dt = new DateTime();
				DateTimeFormatter fmt = DateTimeFormat.forPattern("yyyy-MM-dd HH:mm:ss.S");
				String createdDate = fmt.print(dt);
				user.setAuthTokenCreatedDate(fmt.parseLocalDateTime(createdDate));
				if(!regId.isEmpty() && regId != null){
					user.setRegId(regId);
				}
				service.updateAuthToken(user);
			}
			return new LoginStatus(auth.isAuthenticated(), user);
		} catch (Exception e) {
			return new LoginStatus(false, null);
		}
	}

	public class LoginStatus {

		private final boolean loggedIn;
		private final User user;

		public LoginStatus(boolean loggedIn, User user) {
			this.loggedIn = loggedIn;
			this.user = user;
		}

		public boolean isLoggedIn() {
			return loggedIn;
		}

		public User getUser() {
			return user;
		}
	}

}
