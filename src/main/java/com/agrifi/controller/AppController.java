package com.agrifi.controller;

import com.agrifi.model.*;
import com.agrifi.service.ZELServices;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.core.env.Environment;
import org.springframework.data.mongodb.core.MongoOperations;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.scheduling.TaskScheduler;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;

import javax.servlet.http.HttpServletRequest;
import javax.sound.sampled.UnsupportedAudioFileException;
import javax.validation.Valid;
import java.io.IOException;
import java.util.List;
import java.util.concurrent.ScheduledFuture;

@ComponentScan({ "com.agrifi" })
@Component
@Controller
@EnableWebMvc
@RequestMapping("/")
public class AppController {

	@Autowired
	@Qualifier("ZELServices")
	ZELServices service;

	@Autowired
	private TaskScheduler scheduler;

	private ScheduledFuture job;


	@Autowired
	private Environment environment;
	/*
	 * This method will list all existing employees.
	 */

	@RequestMapping(value={"/","getAllLoanDetails"}, method = RequestMethod.GET)
	public String getAllLoanDetails(ModelMap model){

		Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
		String userEmail  = authentication.getName();
		User user = service.getUserByEmail(userEmail);
		if(user.getRoles().equals("AGENT")){
		List<LoanDetailsNew> loanDetailsList = service.getAllLoanDetails();
		model.addAttribute("loanDetailsList", loanDetailsList);
		return "loanDetailsList";
	} else if ((user.getRoles()).equals("ADMIN")) {
				List<LoanDetailsNew> qualifiedList=service.getQualifiedList();
			model.addAttribute("qualifiedList", qualifiedList);
			return "admin";
		} else if ((user.getRoles()).equals("BANK")) {
			List<LoanDetailsNew> qualifiedBankList=service.getForwardedBankList();
			model.addAttribute("qualifiedBankList", qualifiedBankList);
			return "banks";
		}

		return "login";
	}

	@RequestMapping(value = {"/update-internal-status"},method = RequestMethod.POST)
	public String updateInternalStatus(@RequestParam(value = "loanId") String loanId,ModelMap map){
	int loanDetailsList=service.updateInternalStatus(loanId,"Qualified");
		map.addAttribute("loanDetailsList", loanDetailsList);
		return "loanDetailsList";
	}

	@RequestMapping(value = {"/reject-internal-status"},method = RequestMethod.POST)
	public String rejectInternalStatus(@RequestParam(value = "loanId") String loanId, ModelMap map){
		int loanDetailsList=service.updateInternalStatus(loanId,"Rejected");
		map.addAttribute("loanDetailsList", loanDetailsList);
		return "loanDetailsList";
	}

	@RequestMapping(value = {"/update-internal-bank-status"},method = RequestMethod.POST)
	public String updateInternalBankStatus(@RequestParam(value = "loanId") String loanId,@RequestParam(value = "bankId") String bankId,ModelMap map){
	int loanDetailsList=service.updateInternalBankStatus(loanId,"Forwarded to Bank",bankId);
		map.addAttribute("loanDetailsList", loanDetailsList);
		return "admin";
	}

	@RequestMapping(value = {"/accept-by-bank"},method = RequestMethod.POST)
	public String acceptByBank(@RequestParam(value = "loanId") String loanId,ModelMap map){
		int loanDetailsList=service.updateInternalStatus(loanId,"Accepted By Bank");
		map.addAttribute("loanDetailsList", loanDetailsList);
		return "banks";
	}

//	Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
//	String bankName  = authentication.getName();
//	BankDetails bank = service.getByBank(bankName);
//		if ((bank.getBankName()).equals("ICICI BANK")){
//		List<LoanDetails> loanDetailsList=service.acceptByICICIBank(loanId,"Accepted By ICICI Bank");
//		map.addAttribute("loanDetailsList", loanDetailsList);
//		return "banks";
//
//	}

	@RequestMapping(value = {"/reject-by-bank"},method = RequestMethod.POST)
	public String rejectByBank(@RequestParam(value = "loanId") String loanId,
							   ModelMap map){
		//service.addRemark(loanId,"");
	int loanDetailsList=service.updateInternalStatus(loanId,"Rejected By Bank");
		map.addAttribute("loanDetailsList", loanDetailsList);
		return "banks";
	}

	@RequestMapping(value = {"/add-remark"},method = RequestMethod.POST)
	public String addRemark(@RequestParam(value = "loanId") String loanId,@RequestParam(value = "addRemark") String addRemark){
		ModelMap map=new ModelMap();
		int loanDetailsList=service.addRemark(loanId,addRemark);
		map.addAttribute("loanDetailsList", loanDetailsList);
		return "banks";
	}


	@RequestMapping(value={"/login"}, method = RequestMethod.POST)
	public String main1(ModelMap model){
		return "login";
	}

	@RequestMapping(value={"/loggedout"}, method = RequestMethod.GET)
	public String loggedout(ModelMap model){
		return "redirect:login";
	}

	@RequestMapping(value={"/done"}, method = RequestMethod.GET)
	public String doneList(ModelMap model){

		Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
		String userEmail  = authentication.getName();
		User user = service.getUserByEmail(userEmail);

		List<LoanDetailsNew> marketingEngineList = service.getAllLoanDetails();

		model.addAttribute("marketingEngineList", marketingEngineList);
		return "admin";
	}


	@RequestMapping(value={"/reject"}, method = RequestMethod.GET)
	public String rejectList(ModelMap model){

		Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
		String userEmail  = authentication.getName();
		User user = service.getUserByEmail(userEmail);

		List<LoanDetailsNew> marketingEngineList = service.getAllLoanDetails();

		model.addAttribute("marketingEngineList", marketingEngineList);
		return "banks";
	}

	@RequestMapping(value={"/admin"}, method = RequestMethod.GET)
	public String admin(ModelMap model){

		/*Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
		String userEmail  = authentication.getName();
		User user = service.getUserByEmail(userEmail);*/

		List<User> users = service.getUserList();
		model.addAttribute("users", users);

		return "admin";
	}

	@RequestMapping(value={"/uploadTestData"}, method = RequestMethod.GET)
	public String uploadTestData(ModelMap model){

		return "uploadTestData";
	}

	@RequestMapping(value ={"/allBankList"},method = RequestMethod.GET)
	public String getAllBank(ModelMap model){
		List<BankDetails> details=service.getBankList();
		model.addAttribute("details", details);
		return "allBankList";
	}

	@RequestMapping(value ={"/bankListById"},method = RequestMethod.GET)
	public String getBankById(@RequestParam("id") String bankId,ModelMap model){
		BankDetails details=service.getBankById(bankId);
		model.addAttribute("details", details);
		return "bankListById";
	}



/*

	@RequestMapping(value = {"/excelProfileUpdate"}, method = RequestMethod.POST)
	public @ResponseBody
	String saveBroadcastList(@RequestParam("file") MultipartFile file, ModelMap model) throws IOException, InvalidFormatException {
		Workbook workbook = WorkbookFactory.create(file.getInputStream());
		Sheet sheet = workbook.getSheetAt(0);

		Iterator<Row> rowIterator = sheet.rowIterator();
		Integer j = 0;
		while (rowIterator.hasNext()) {
			Row row = rowIterator.next();
			ProfSimp profile = new ProfSimp();
			DataFormatter dataFormatter = new DataFormatter();
			// Now let's iterate over the columns of the current row
			if (j > 0) {
				Iterator<Cell> cellIterator = row.cellIterator();
				Integer i = 0;
				while (cellIterator.hasNext()) {
					Cell cell = cellIterator.next();
					String cellValue = dataFormatter.formatCellValue(cell);
					if (i == 0) {
						if (!cellValue.isEmpty()) {
							Long parsedValue = Long.parseLong(cellValue);
							profile.setnumber(parsedValue);
							i++;
							continue;
						}
					}
					if (i == 1) {
						if (!cellValue.isEmpty()) {
							profile.setfarmer_name(cellValue);
							i++;
							continue;
						} else {
							i++;
							continue;
						}
					}
					if (i == 2) {
						if (!cellValue.isEmpty()) {
							profile.settype_of_land(cellValue);
							i++;
							continue;
						} else {
							i++;
							continue;
						}
					}
					if (i == 3) {
						if (!cellValue.isEmpty()) {
							profile.setirrigatioon_kh(cellValue);
							i++;
							continue;
						} else {
							i++;
							continue;
						}
					}
					if (i == 4) {
						if (!cellValue.isEmpty()) {
							profile.setirrigatioon_rabi(cellValue);
							i++;
							continue;
						}  else {
							i++;
							continue;
						}
					}
					if (i == 5) {
						if (!cellValue.isEmpty()) {
							profile.setphone_type(cellValue);
							i++;
							continue;
						}  else {
							i++;
							continue;
						}
					}
					if (i == 6) {
						if (!cellValue.isEmpty()) {
							profile.setgender(cellValue);
							i++;
							continue;
						}  else {
							i++;
							continue;
						}
					}
					if (i == 7) {
						if (!cellValue.isEmpty()) {
							profile.setdistrict(cellValue);
							i++;
							continue;
						}  else {
							i++;
							continue;
						}
					}
					if (i == 8) {
						if (!cellValue.isEmpty()) {
							profile.setblock(cellValue);
							i++;
							continue;
						}  else {
							i++;
							continue;
						}
					}
					if (i == 9) {
						if (!cellValue.isEmpty()) {
							profile.setpanchayat(cellValue);
							i++;
							continue;
						}  else {
							i++;
							continue;
						}
					}
					if (i == 10) {
						if (!cellValue.isEmpty()) {
							profile.setvillage(cellValue);
							i++;
							continue;
						}  else {
							i++;
							continue;
						}
					}
					if (i == 11) {
						if (!cellValue.isEmpty()) {
							profile.setkharif_crop_1(cellValue);
							i++;
							continue;
						}  else {
							i++;
							continue;
						}
					}
					if (i == 12) {
						if (!cellValue.isEmpty()) {
							profile.setkharif_crop_2(cellValue);
							i++;
							continue;
						}  else {
							i++;
							continue;
						}
					}
					if (i == 13) {
						if (!cellValue.isEmpty()) {
							profile.setkharif_crop_3(cellValue);
							i++;
							continue;
						}  else {
							i++;
							continue;
						}
					}
					if (i == 14) {
						if (!cellValue.isEmpty()) {
							profile.setrabi_crop_1(cellValue);
							i++;
							continue;
						}  else {
							i++;
							continue;
						}
					}
					if (i == 15) {
						if (!cellValue.isEmpty()) {
							profile.setrabi_crop_2(cellValue);
							i++;
							continue;
						}  else {
							i++;
							continue;
						}
					}
					if (i == 16) {
						if (!cellValue.isEmpty()) {
							profile.setrabi_crop_3(cellValue);
							i++;
							continue;
						}  else {
							i++;
							continue;
						}
					}
				}

			}
			service.updateProfSimp(profile);
			j++;
		}
		return "Success";
	}
*/



	@RequestMapping(value = {"/markCallDone"}, method = RequestMethod.POST)
	public @ResponseBody
	void markCallDone(@RequestParam("id") String id, ModelMap model) throws IOException, UnsupportedAudioFileException {
		service.markCallDone(id);
	}


	@RequestMapping(value = {"/saveUser"}, method = RequestMethod.POST)
	public @ResponseBody
	User saveUser(@Valid User user, BindingResult result, HttpServletRequest request, ModelMap model) {

		User existingUser = new User();

		if(!user.getEmailId().isEmpty() && user.getEmailId()!=null){
			existingUser = service.getUserByEmailOrPhone(user.getContactNumber(), user.getEmailId());
		} else {
			existingUser = service.getUserByEmailOrPhone(user.getContactNumber(), user.getContactNumber());
		}

		if (existingUser != null && !user.getContactNumber().isEmpty()) {
			return existingUser;
		} else {
			User newUser = service.saveUser(user);
			return newUser;
		}
	}




}