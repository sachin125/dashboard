package com.agrifi.service;

import com.agrifi.dao.ZELDao;
import com.agrifi.model.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Component
@Qualifier("ZELServices")
@Service("ZELServices")
@Transactional("transactionManager")
public class ZELServiceImpl implements ZELServices {

	@Autowired
	private ZELDao dao;

	@Override
	public User getUserByEmail(String userId) {
		return dao.getUserByEmail(userId);
	}


	@Override
	public BankDetails getByBank(String bankId) {
		return dao.getByBank(bankId);
	}

	@Override
	public User getUserById(String id) {
		System.out.println("***** 2");
		return dao.getUserById(id);
	}

	@Override
	public void updateUserCompany(User user) {
		dao.updateUserCompany(user);
	}

	@Override
	public void updateAuthToken(User user) {
		dao.updateAuthToken(user);
	}

	@Override
	public User getUserByToken(String strToken) {
		return dao.getUserByToken(strToken);
	}

	@Override
	public User getUserByEmailOrPhone(String emailId, String contactNumber) {
		return dao.getUserByEmailOrPhone(emailId,contactNumber);
	}

	@Override
	public User saveUser(User user) {
		return dao.saveUser(user);
	}

	@Override
	public List<LoanDetailsNew> getAllLoanDetails() {
		return dao.getAllLoanDetails();
	}

	@Override
	public void markCallDone(String id) {
		dao.markCallDone(id);
	}

	@Override
	public List<User> getUserList() {
		return dao.getUserList();
	}


	@Override
	public List<LoanDetailsNew> getQualifiedList() {
		return this.dao.getQualifiedList();
	}

	@Override
	public List<LoanDetailsNew> getForwardedBankList(){
		return dao.getForwardedBankList();
	}

	@Override
	public int updateInternalStatus(String loanId,String internalStatus) {
		return dao.updateInternalStatus(loanId,internalStatus);
	}

	@Override
	public List<LoanDetails> rejectInternalStatus(String loanId,String internalStatus) {
		return this.dao.rejectInternalStatus(loanId,internalStatus);
	}

	@Override
	public int updateInternalBankStatus(String loanId, String internalStatus,String bankId) {
		return this.dao.updateInternalBankStatus(loanId,internalStatus,bankId);
	}


	@Override
	public List<LoanDetails> acceptByBank(String loanId,String internalStatus) {
		return this.dao.acceptByBank(loanId,internalStatus);
	}


	@Override
	public List<LoanDetails> acceptByICICIBank(String loanId,String internalStatus) {
		return this.dao.acceptByICICIBank(loanId,internalStatus);
	}

	@Override
	public List<LoanDetails> rejectByBank(String loanId, String internalStatus) {
		return dao.rejectByBank(loanId,internalStatus);
	}

	@Override
	public int addRemark(String loanId,String addRemark) {
		return this.dao.addRemark(loanId,addRemark);
	}


	@Override
	public List<BankDetails> getBankList(){
		return this.dao.getBankList();
	}

	@Override
	public BankDetails getBankById(String bankId){
		return this.dao.getBankById(bankId);
	}

	@Override
	public List<MarketingEngine> getMEDataByRoleStatus1(String roles, boolean b) {
		return dao.getMEDataByRoleStatus1(roles,b);
	}
}