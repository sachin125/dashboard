package com.agrifi.service;


import com.agrifi.dao.ZELDao;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Transactional("transactionManager")
@Service("userDetailsService")
public class LoginServiceImpl implements UserDetailsService {

	@Autowired
	private ZELDao dao;

	@Override
	public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException,DataAccessException {
		System.out.println("************* here 3"+username);
		System.out.println("************* here 4"+dao.loadUserByUserName(username).getAuthorities());
		return dao.loadUserByUserName(username);
	}

}
