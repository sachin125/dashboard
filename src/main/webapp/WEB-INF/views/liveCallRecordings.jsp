<%@ page contentType="text/html; charset=UTF-8" %>
<%@ page pageEncoding="UTF-8" %>

<%@taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html>
<html>
<head>
  <title>Krishi Tarang - IVR Servicess</title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">

  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">

  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/sweetalert/2.1.0/sweetalert.min.js"></script>
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/jquery-datetimepicker/2.5.20/jquery.datetimepicker.css" />
  <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-datetimepicker/2.5.20/jquery.datetimepicker.full.js"></script>
  <script src="resources/js/broadcastList.js"></script>

  <style>
  /* Note: Try to remove the following lines to see the effect of CSS positioning */
  .affix {
      top: 0;
      width: 100%;
      z-index: 9999 !important;
  }

  .affix + .container-fluid {
      padding-top: 70px;
  }
  .margin-top-10 {
    margin-top:10px;
  }
  .margin-top-5 {
    margin-top:5px;
  }

  .border-box {
      border: 1px solid #ccc;
      padding: 20px;
      margin: 10px;
  }


       .margin-top-15 {
          margin-top : 15px;
         }

       #annList {
        font-family: "Trebuchet MS", Arial, Helvetica, sans-serif;
        border-collapse: collapse;
        width: 100%;
      }

      #annList td, #annList th {
        border: 1px solid #ddd;
        padding: 8px;
      }

      #annList tr:nth-child(even){background-color: #f2f2f2;}

      #annList tr:hover {background-color: #ddd;}

      #annList th {
        padding-top: 12px;
        padding-bottom: 12px;
        text-align: left;
        background-color: #4CAF50;
        color: white;
      }

  </style>

<script type="text/javascript">

    $(document).ready(function(){

    $(".selectFarmer").change(function() {
        var checkedVals = $('.selectFarmer:checkbox:checked').map(function() {
            return this.value;
        }).get();
        $("#numbers").val(checkedVals.join(","));

        var checkeddata = $('.selectFarmer:checkbox:checked').map(function() {
            return $(this).attr("data");
        }).get();
        $("#selectIds").val(checkeddata.join(","));
    })



    $("#answer-audio").click(function(){

    var data1 = new FormData();
        data1.append('file', $('#uploadAndSendAnswersFile input[type=file]')[0].files[0]);
        data1.append('numbers',$("#numbers").val());
        var selected_file = $.trim( $('#uploadAndSendAnswersFile input[type=file]').val());

        if (selected_file != "") {
            console.log(data1);
            $.ajax({
                url: "uploadAndSendLiveCallAnswers",
                type: "POST",
                contentType: false,
                processData: false,
                cache: false,
                data: data1,
                success: function(response) {

                    var data2 = new FormData();
                     data2.append('qids',$("#selectIds").val());

                    $.ajax({
                        url: "markLiveCallsQAnswered",
                        type: "POST",
                        contentType: false,
                        processData: false,
                        cache: false,
                        data: data2,
                        success: function(response) {
                        console.log("answer marked")
                        swal("Success!!", "Published!", "success");
                        setTimeout(function(){  location.reload(); }, 2000);
                        },
                        error: function() {
                        }
                    });


                },
                error: function() {
                }
            });
            $('#uploadAndSendAnswersFile input[type=file]').val('');
        } else if(selected_file == ""){
                swal("Failed!!", "Failed!", "error");
                setTimeout(function(){  location.reload(); }, 2000);
        }

    });
    });


</script>
</head>
<body>

<div class="container-fluid" style="background-color:#52AE23;color:#fff;">
    <h1>Krishi Tarang - IVR Service</h1>
</div>

<nav class="navbar navbar-inverse" data-spy="affix" data-offset-top="197">
    <ul class="nav navbar-nav">
        <li class="active"><a href="liveCallRecordings">Live Call Logs</a></li>
        <li class=""><a href="liveCallAnswers">Live Call Answer Logs</a></li>

    </ul>
</nav>

<div class="border-box">
<h3>Answer Questions</h3>
<div class="container-fluid border-box">

<div class="col-md-4">
    <form name="uploadAndSendAnswers" id="uploadAndSendAnswers" method="POST" action="saveAnswerAudio">
        <label class="margin-top-5">Selected Farmers</label>
        <input type="text" name="numbers" id="numbers"  class="form-control" title="Please Enter Valid Audio Name" required>
         <input type="hidden" value="" name="selectIds" id="selectIds">

    </form>
</div>
<div class="col-md-3">
    <form method="POST" id="uploadAndSendAnswersFile" action="uploadAndSendAnswers" enctype="multipart/form-data">
        <label class="margin-top-5">Please select a audio file to Answer</label>
        <input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}"/>
        <input type="file" name="fileName" class="fileName" id="fileName" style="height:30px"/>
        <input type="hidden" name="referenceId" value=""/>
    </form>
</div>
<div class="col-md-3 margin-top-5">
    <div type="submit" class="margin-top-5 btn btn-primary btn-lg"  id="answer-audio" >Answer</div>
</div>
</div>


</div>

<div class="border-box">
    <table id="annList" style="width:100%" class="annList">
        <h3>Live Call center Log</h3>
        <tr>
            <th>Select Farmer</th>
            <th>Call From</th>
            <th>Call To</th>
            <th>Call Status</th>
            <th>Direction</th>
            <th>Call Created</th>
            <th>Dial Call Duration</th>
            <th>Recording Url</th>
            <th>Status</th>
        </tr>
             <c:forEach items="${passThroughs}" var="pass">
                <tr>
                    <td>
                         <input type="checkbox" class="selectFarmer" name="selectFarmer" data="${pass.id}" value="${pass.from}">
                    </td>
                    <td>
                        ${pass.from}
                    </td>
                    <td>
                        ${pass.to}
                    </td>
                    <td>
                        ${pass.callStatus}
                     </td>
                    <td>
                        ${pass.direction}
                    </td>
                    <td>
                        ${pass.created}
                    </td>
                    <td>
                        ${pass.dialCallDuration} secs
                    </td>
                    <td>
                        <audio controls>
                          <source src="${pass.recordingUrl}" type="audio/mpeg">
                        Your browser does not support the audio element.
                        </audio>
                    </td>
                    <td>
                      <c:choose>
                          <c:when test="${pass.answered == true}">
                              <div style="color:green">Answered<div>
                          </c:when>
                          <c:when test="${pass.answered == false}">
                              <div style="color:red">Not Answered<div>
                          </c:when>
                      </c:choose>
                    </td>
                </tr>
            </c:forEach>
    </table>

</div>

</body>
</html>
