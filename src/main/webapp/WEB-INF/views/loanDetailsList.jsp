<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" isELIgnored="false"%>

<%@taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html>
<html>
<head>
  <title>AGRIFI LOAN LIST</title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">

  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">

  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/sweetalert/2.1.0/sweetalert.min.js"></script>
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/jquery-datetimepicker/2.5.20/jquery.datetimepicker.css" />
  <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-datetimepicker/2.5.20/jquery.datetimepicker.full.js"></script>
  <script src="resources/js/broadcastList.js"></script>

<script type="text/javascript">


    $(document).ready(function(){

    $('.tag-btn').click(function(){

            var data = new FormData();
            data.append('loanId',$(this).attr("id"));
            $.ajax({
                url: "update-internal-status",
                type: "POST",
                contentType: false,
                processData: false,
                cache: false,
                data: data,
                success: function(response) {
                swal("Success!!", "Saved!", "success");
                setTimeout(function(){  location.reload(); }, 1000);
                },
                error: function() {
                }
            });
        });
    });



    $(document).ready(function(){
        $('.tag-btns').click(function(){

                var data = new FormData();
                data.append('loanId',$(this).attr("id"));
                    $.ajax({
                    url: "reject-internal-status",
                    type: "POST",
                    contentType: false,
                    processData: false,
                    cache: false,
                    data: data,
                    success: function(response) {
                    swal("Success!!", "Saved!", "success");
                    setTimeout(function(){  location.reload(); }, 1000);
                    },
                    error: function() {
                    }
                });
            });
        });


        $(document).ready(function(){
          $("#myInput").on("keyup", function() {
            var value = $(this).val().toLowerCase();
            $("#annList tr").filter(function() {
              $(this).toggle($(this).text().toLowerCase().indexOf(value) > -1)
            });
          });
        });

</script>

  <style>
  /* Note: Try to remove the following lines to see the effect of CSS positioning */
  .affix {
      top: 0;
      width: 100%;
      z-index: 9999 !important;
  }

  .affix + .container-fluid {
      padding-top: 70px;
  }
  .margin-top-10 {
    margin-top:10px;
  }
  .margin-top-5 {
    margin-top:5px;
  }




       .margin-top-15 {
          margin-top : 15px;
         }

       #annList {
        font-family: "Trebuchet MS", Arial, Helvetica, sans-serif;
        border-collapse: collapse;
        width: 100%;
      }

      #annList td, #annList th {
        border: 1px solid #ddd;
        padding: 8px;
      }

      #annList tr:nth-child(even){background-color: #f2f2f2;}

      #annList tr:hover {background-color: #ddd;}

      #annList th {
        padding-top: 12px;
        padding-bottom: 12px;
        text-align: left;
        background-color: #4CAF50;
        color: white;
      }

   .form-control{
                     margin-top:10px;
                     width:-80px;
                    }
                    .btn-block{
                    margin-top:10px;
                      width:30%;
                     }

                     .tag-btn{
                           background-color: #348C31;
                            margin-top:10px;
                           }
                           .tag-btns{
                           background-color: #FF0000;
                           margin-top:10px;
                           }
                           .btn-primary{
                          margin-top:10px;
                          background-color: #348C31;

                           }

                           .btn-secondary{
                           background-color:#ff0000;

                           }


   .tag-btn{
         background-color: #348C31;
          margin-top:10px;
         }
         .tag-btns{
         background-color: #FF0000;
         margin-top:10px;
         }
         .btn-primary{
        margin-top:10px;
        background-color: #348C31;

         }

         .btn-secondary{
         background-color:#ff0000;

         }





  </style>
</head>
<body>

<div class="container-fluid" style="background-color:#52AE23;color:#fff;">
    <h2>AGRIFI LOAN LIST</h2>
</div>
<nav class="navbar navbar-inverse" data-spy="affix" data-offset-top="197">
    <ul class="nav navbar-nav">
        <li class="active"><a href="getAllLoanDetails">Loan List</a></li>
          <li class="active" style="position:absolute;right:15px"><a href="logout">Logout</a></li>
 </ul>
<div class="col-md-6">
  <input class="form-control" id="myInput" type="text" placeholder="Search..">
   </div>
 </nav>
<div class="border-box">
    <table id="annList" style="width:100%;font-size: 12px;" class="annList">
             <tr>
                   <th>LoanId</th>
                   <th>Name</th>
                   <th>Contact Number</th>
                    <th>Loan Type</th>
                     <th>Bank Name</th>
                     <th>Date</th>
                     <th>Pan Number</th>
                     <th>Aadhaar Number</th>
                    <th>Aadhaar Front Image</th>
                     <th>Aadhaar Back Image</th>
                     <th>Pan Image</th>
                     <th>Land Document</th>
                     <th>GST Image</th>
                     <th>Loan Status</th>
                      <th>Action</th>
              </tr>

                    <c:forEach items="${loanDetailsList}" var="l">
              <tr>
                           <td>
                               ${l.loanId}
                           </td>
                      <td>
                               ${l.name}
                           </td>
                            <td>
                                                          ${l.contactNumber}
                                                      </td>
                           <td>
                                 ${l.loanType}
                             </td>
                         <td>
                                  ${l.bankName}
                             </td>
                       <td>
                             ${l.createdOn.toString().substring(0, l.createdOn.toString().indexOf(' '))}
                                                  </td>
                       <td>
                                   ${l.panNo}
                                  </td>
                        <td>
                                   ${l.aadhaarNo}
                                 </td>
                       <td>
                                                                           <a href="${l.aadhaarFrontLink}" data-target="#${l.loanId}"data-toggle="modal" >Show Image</a>

       <td>
          ${l.name}
             </td>

           <td>
         ${l.loanType}
          </td>

          <td>
           ${l.bankName}
         </td>

               <td>
              ${l.createdOn.toString().substring(0, l.createdOn.toString().indexOf(' '))}
                  </td>

           <td>
         ${l.panNo}
            </td>

                 <td>
           ${l.aadhaarNo}
         </td>

            <td>
            <a href="${l.aadhaarFrontLink}" data-target="#${l.loanId}"data-toggle="modal" >Show Image</a>
             <div id="${l.loanId}" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
            <div class="modal-dialog">
             <div class="modal-content">
             <div class="modal-body">
         <img src="${l.aadhaarFrontLink}" class="img-responsive">
             </div>
           </div>
            </div>
           </div>
           </td>

            <td>
   <a href="${l.aadhaarBackLink}" data-target="#${l.loanId}"data-toggle="modal" >Show Image</a>
  <div id="${l.loanId}" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog">
     <div class="modal-content">
    <div class="modal-body">
   <img src="${l.aadhaarBackLink}" class="img-responsive">
    </div>
    </div>
   </div>
    </div>
 </td>

            <td>
   <a href="${l.panLink}" data-target="#${l.loanId}"data-toggle="modal" >Show Image</a>
 <div id="${l.loanId}" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
     <div class="modal-dialog">
    <div class="modal-content">
    <div class="modal-body">
     <img src="${l.panLink}" class="img-responsive">
 </div>
  </div>
   </div>
   </div>
</td><td>
                                                      <div id="${l.loanId}" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                                                        <div class="modal-dialog">
                                                          <div class="modal-content">
                                                              <div class="modal-body">
                                                                  <img src="${l.landDocumentLink}" class="img-responsive">
                                                              </div>
                                                          </div>
                                                        </div>
                                                      </div>
                              </td>
                                       <td>
                                                                                        <a href="${l.gstCertificateLink}" data-target="#${l.loanId}" data-toggle="modal" >Show Image</a>

                                                                                      <div id="${l.loanId}" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                                                                                        <div class="modal-dialog">
                                                                                          <div class="modal-content">
                                                                                              <div class="modal-body">
                                                                                                  <img src="${l.gstCertificateLink}" class="img-responsive">
                                                                                              </div>
                                                                                          </div>
                                                                                        </div>
                                                                                      </div>

                                                                       </td>

                            <td>
                                    ${l.internalStatus}
                               </td>
                                           <td>
                                           <button class="btn btn-primary tag-btn approveBtn" id="${l.loanId}" data="${l.loanId}" type="button" onclick="window.location.reload(true)">Approve</button>
                                           <button class="btn btn-secondary tag-btns rejectBtn" id="${l.loanId}" data="${l.loanId}" type="button" onclick="window.location.reload(true)">Reject</button>
                                                                           </td>
                 </tr>
            </c:forEach>
    </table>
>>>>>>> ad7679029ef33c9cf35e52ef8bc76c9ffbda2927

           <td>
             <a href="${l.landDocumentLink}" data-target="#${l.loanId}"data-toggle="modal" >Show Image</a>
<div id="${l.loanId}" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
<div class="modal-dialog">
<div class="modal-content">
   <div class="modal-body">
  <img src="${l.landDocumentLink}" class="img-responsive">
</div>
</div>
 </div>
 </div>
</td>

 <td>
<a href="${l.gstCertificateLink}" data-target="#${l.loanId}" data-toggle="modal" >Show Image</a>
 <div id="${l.loanId}" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
 <div class="modal-dialog">
 <div class="modal-content">
 <div class="modal-body">
  <img src="${l.gstCertificateLink}" class="img-responsive">
</div>
  </div>
 </div>
</div>
  </td>

        <td>
         ${l.internalStatus}
            </td>
<td>
<button class="btn btn-primary tag-btn approveBtn" id="${l.loanId}" data="${l.loanId}" type="button" onclick="window.location.reload(true)">Approve</button>
<button class="btn btn-secondary tag-btns rejectBtn" id="${l.loanId}" data="${l.loanId}" type="button" onclick="window.location.reload(true)">Reject</button>
                                </td>

 </tr>
</c:forEach>
 </table>
</div>
</body>
</html>